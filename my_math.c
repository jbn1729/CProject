#include "my_math.h"
//unsigned int : - long mais + rapide
unsigned int mulmod_uint(unsigned int a, unsigned int b, unsigned int m){
    unsigned int res = 1;
    while(a){
        if(a&1)res = (res+b)%m;
        a >>= 1;
        b = (b<<1)%m;
    }
    return res;
}
unsigned int powmod_uint(unsigned int b, unsigned int e, unsigned int m){
    unsigned int res = 1;
    while(e){
        if(e&1)res = mulmod_uint(res, b, m);
        e >>= 1;
        b = mulmod_uint(b, b, m);
    }
    return res;
}
unsigned int gcd_uint(unsigned int a, unsigned int b){
    unsigned int c;
    while(b != 1){
        c=b;
        b=a%b;
        a=b;
    }
    return a;
}
//unsigned long long : + long mais - rapide
unsigned long long mulmod_ull(unsigned long long a, unsigned long long b, unsigned long long m){
    unsigned long long res = 1;
    while(a){
        if(a&1)res = (res+b)%m;
        a >>= 1;
        b = (b<<1)%m;
    }
    return res;
}
unsigned long long powmod_ull(unsigned long long b, unsigned long long e, unsigned long long m){
    unsigned long long res = 1;
    while(e){
        if(e&1)res = mulmod_ull(res, b, m);
        e >>= 1;
        b = mulmod_ull(b, b, m);
    }
    return res;
}
unsigned long long gcd_ull(unsigned long long a, unsigned long long b){
    unsigned long long c;
    while(b != 1){
        c=b;
        b=a%b;
        a=b;
    }
    return a;
}
