#include <stdio.h>
#include <math.h>

unsigned long long dist3(unsigned long long x){
    double y=(double)(x);
    unsigned long long i;
    for(i=0; y!=4; i++) y=2+sqrt(y);
    return i;

}
unsigned long long dist3xmx(unsigned long long x){
    return dist3(x)-dist3(x-1);
}
unsigned char booleen(unsigned long long x, unsigned long long attempt){
    unsigned long long xd = dist3(x);
    if (xd==attempt)
        return 0;
    else if (xd==dist3(x+1))
        return 1;
    else return 2;
}

unsigned long long quick_search(unsigned long long x, unsigned long long bm, unsigned long long bp){
    unsigned long long r = bp-1, m, g = bm;
    unsigned char cond;
    while(r >= g){
        m = (r + g) >> 1;
        cond = booleen(m, x);
        if(cond == 0){
            g = m + 1;
        }else if(cond==1){
            r = m - 1;
        }else{
            return m;
        }
    }
    return g;
}

int main(){
    unsigned long long i = 9, bm=8, bp=16, res=27, newi;
    printf("0 1\n26 4\n");
    for(int _loop=0; bm<bp; _loop++){
        if(dist3(bp) > res){
            //printf("trouvé l'intervalle : %lld %lld\n", bm, bp);
            newi = quick_search(res, bm, bp);
            printf("%d %d\n", res, newi-i);
            i = newi;
            res = dist3(i);
        }else{
            //printf("bm = %lld, bp = %lld\n", bm, bp);
            bm <<= 1;
            bp <<= 1;
        }
    }
}
