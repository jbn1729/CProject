#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/sha.h>

void llu_to_char(unsigned long long n, unsigned char* R){
    for (int i = 0; i < 8; ++i)
        R[i] = (unsigned char)((n >> (56 - (i<<3))) & 0xFFu);
}
void sha256(unsigned long long n, unsigned char* hash){
	unsigned char data[8];
	llu_to_char(n, data);
	SHA256(data, strlen((char *)(&data)), hash);
}

int main(int argc, char* argv[]) {
	FILE *fptr;
	FILE *fptrw;
	/*char file[strlen(argv[0])];
	strcpy(file, argv[0]);*/
	char *eptr;
	unsigned long long key = strtoull(argv[2], &eptr, strlen(argv[2]));
	printf("%llu\n", key);
	char chiffre=1;
	if(argc > 4)
		chiffre=0;
	// Open a file in read mode
	fptr = fopen(argv[1], "rb");
	fptrw = fopen(argv[3], "wb");

	// Store the content of the file
	char c;
	unsigned char hash[SHA256_DIGEST_LENGTH];
	// If the file exist
	if(fptr != NULL) {
		// Read the content and print it
		while((c=fgetc(fptr)) != EOF){
			//printf("%c : ", c);
			sha256(key, hash);
			if(chiffre)
				c += hash[SHA256_DIGEST_LENGTH-1];
			else
				c -= hash[SHA256_DIGEST_LENGTH-1];
			key ++;
			fputc(c, fptrw);
			//printf("%c\n", c);
		}

	// If the file does not exist
	} else {
		printf("Not able to open the file.");
	}
	// Close the file
	fclose(fptr);
}
