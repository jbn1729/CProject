#include <math.h>
#include <complex.h>
#include <stdlib.h>
#include <stdio.h>
#define MAXI(a, b) a<b?b:a
#define MINI(a, b) a<b?a:b
#define IMAX(a, b) a<b?1:0
#define MAXW 16384
#define MAXH 16384
#define DIST_MAX 65536
#define PI 3.14159265358979323846264338327950
//liste 1D et liste 3D

typedef struct{
    unsigned char list[MAXW*MAXH*3];
    unsigned int length;
}Liste1D;
typedef struct{
    unsigned char list[MAXW][MAXH][3];
    unsigned int maxw;
    unsigned int maxh;
}Liste3D;
//nombres complexes
#define UN CMPLX(1, 0)
#define ZE CMPLX(0, 0)
//formule
double complex newton_fonc(double complex x, double complex* fonc, unsigned int lfonc){
    /*
    fonc(x) = (x-fonc[0])*(x-fonc[1])...
    return fonc(x)/fonc'(x)
    */
    double complex s=0, xmri;
    for(unsigned int i=0; i<lfonc; i++){
        xmri = x-fonc[i];
        //if(cabs(xmri) < 1e-10)return i;
        s += 1/xmri;
    }
    return 1/s;
}
//polynomes

void derivate(double complex* der, double complex* poly, unsigned int lpoly){
    for(int i=1;i<lpoly;i++){
        der[i-1] = i*poly[i];
    }
}
double complex evalPC(double complex *poly, double complex c, unsigned int lpoly){
    double complex puic = UN;
    double complex res = ZE;
    for(int i=0; i < lpoly; i++){
        res += puic*poly[i];
        puic *= c;
    }
    return res;
}

double complex evalPdivD(double complex* poly, double complex* der, double complex c, unsigned int lpoly){
    double complex res1 = poly[lpoly-1], res2 = der[lpoly-2];
    for(int i=lpoly-2; i > 0; i--){
        res1 = res1*c+poly[i];
        res2 = res2*c+der[i-1];
    }
    return (res1*c+poly[0])/res2;
}

void create_simplPoD(double complex* H, double complex* B, double complex* pol, unsigned int lpol){
    H[0] = -pol[0];
    H[1] = 0;
    for(int i=lpol-1; i>1; i--){
        H[i] = pol[i]*(i-1);
    }
    derivate(B, pol, lpol);
}

double complex simplPoD(double complex* H, double complex* B, double complex x, unsigned int lpoly){
    unsigned int sepa = lpoly;
    double complex res1 = H[sepa-1],
                   res2 = B[sepa-2];
    for(int i=sepa-2; i > 1; i--){
        res1 = res1*x+H[i];
        res2 = res2*x+B[i-1];
    }
    return (res1*x*x+H[0])/(res2*x+B[0]);
}

void addpoly(double complex* poly_op,   double complex* poly1, double complex* poly2,
                                        unsigned int lpoly1  , unsigned int lpoly2){
    unsigned int maxii = MINI(lpoly1, lpoly2);
    for(int i=0; i<maxii; i++){
        poly_op[i] = poly1[i]+poly2[i];
    }
    unsigned int minii = maxii;
    maxii = MAXI(lpoly1, lpoly2);
    for(unsigned int i=minii; i<maxii; i++){
        if(lpoly1 < lpoly2)
            poly_op[i] = poly2[i];
        else
            poly_op[i] = poly1[i];
    }
}
void mulpolyx(double complex* poly_op, double complex* poly, unsigned int lpoly){
    poly_op[0] = 0;
    for(unsigned int i=1;i<=lpoly;i++){
        *(poly_op+i) = poly[i-1];
    }
}
void oppoly(double complex* poly_op, double complex* poly, unsigned int lpoly){
    for(int i=0;i<lpoly;i++){
        poly_op[i] = -(*(poly+i));
    }
}

void mulpolya(double complex* poly_op, double complex* poly, unsigned int lpoly, double complex a){
    for(int i=0;i<lpoly;i++){
        poly_op[i] = a*(*(poly+i));
    }
}

void copypoly(double complex* poly, double complex* polyc, unsigned int lpolyc){
    for(int i=0;i<lpolyc;i++){
        *(poly+i) = *(polyc+i);
    }
}

void mulpoly(double complex* poly_op, double complex* poly, double complex a, unsigned int lpoly){
    //(2x²+3x+5)*(x-a)
    //[5, 3, 2]*(x-a)
    double complex poly_i[lpoly+1];
    double complex poly_j[lpoly+1];
    mulpolya(poly_i, poly, lpoly, a);
    oppoly(poly_i, poly_i, lpoly);
    mulpolyx(poly_j, poly, lpoly);
    addpoly(poly_op, poly_j, poly_i, lpoly+1, lpoly);
}

void create_poly(double complex* ppoly, double complex* roots, unsigned int nbroots){
    //(x-roots[0])*(x-roots[1])...
    double complex poly[nbroots+1];
    poly[0] = -roots[0];
    poly[1] = UN;
    unsigned int lpoly = 2;
    for(int i=1; i<nbroots; i++){
        mulpoly(poly, poly, *(roots+i), lpoly);
        lpoly++;
    }
    for(int i=0; i<(nbroots+1); i++){
        ppoly[i] = poly[i];
    }
}
//newton
double complex newton(double complex c, unsigned int maxit, unsigned int lpoly, double complex* H, double complex* B, double complex* roots, unsigned int lroots){
    double complex cav;
    for(int i=0; i<maxit; i++){
        cav = c,
        //c-= newton_fonc(c, roots, lroots);
        c = simplPoD(H, B, c, lpoly);
        if (cabs(c-cav)<1e-10) return c;
    }
    return c;
}

double dist(double complex c1, double complex c2){
    return cabs(c1-c2);
}
unsigned int iclose(double complex cp, double complex* cs, unsigned int lcs){
    double distmin = DIST_MAX, distpoint;
    int imin=0;
    for(int i=0; i<lcs; i++){
        distpoint = dist(cp, cs[i]);
        if(distpoint < distmin){
            imin = i;
            distmin = distpoint;
        }
    }
    return imin;
}
unsigned int color_point(double x, double y, unsigned int lpoly, double complex* roots, unsigned int lroots, double complex* H, double complex* B){
    return iclose(newton(CMPLX(x, y), 100, lpoly, H, B, roots, lroots), roots, lroots);
}
//fractal
void copy_liste1D(Liste1D* F, Liste1D* Fc){
    F->length = Fc->length;
    for(int i=0; i<(F->length); i++){
        F->list[i] = Fc->list[i];
    }
}

void remplis(Liste1D* F, unsigned char r, unsigned int imin){
    for(unsigned int i=imin; i<F->length; i++){
        F->list[i] = r;
    }
}

void fractal(  Liste1D* F, unsigned int width, unsigned int height, 
                        double scale, double dec_x, double dec_y,
                        double complex* poly,  unsigned int lpoly,
                        double complex* roots, unsigned int lroots,
                        double complex a){
    double x, y;
                                                    
    unsigned char COLOR[8][3] = {   {255, 0  , 0  },   //#FF0000
                                    {0  , 255, 0  },   //#00FF00
                                    {0  , 0  , 255},   //#0000FF
                                    {255, 255, 0  },   //#FFFF00
                                    {0  , 255, 255},   //#00FFFF
                                    {255, 0  , 255},   //#FF00FF
                                    {255, 255, 255},   //#FFFFFF
                                    {0  , 0  , 0  }};  //#000000
    unsigned int rvbi;
    double complex H[lpoly],
                   B[lpoly-1];
    create_simplPoD(H, B, poly, lpoly);
    #pragma omp parallel for
    for(int i=0; i<width; i++){
        unsigned int ijk=i*height*3;
        x = (i-dec_x)/scale;
        for(int j=0; j<height; j++){
            y = (j-dec_y)/scale;
            rvbi = color_point(x, y, lpoly, roots, lroots, H, B);
            F->list[ijk] = COLOR[rvbi][0];
            F->list[ijk+1] = COLOR[rvbi][1];
            F->list[ijk+2] = COLOR[rvbi][2];
            ijk += 3;
        }
    }
}
void fractal_roots(Liste1D*F, unsigned int nbroots, unsigned int width, unsigned int height, double scale, double dec_x, double dec_y, double complex a){
    double complex pi2i = CMPLX(0, PI*2);//pi*2i
    double complex roots[nbroots];
    F->length = width*height*3;
    for(unsigned int i=0; i < nbroots; i++){
        roots[i] = cexp(pi2i*i/nbroots);
    }
    double complex poly[nbroots+1];
    create_poly(poly, roots, nbroots);
    unsigned int lpoly=nbroots+1;
    fractal(F, width, height, scale, dec_x, dec_y, poly, lpoly, roots, nbroots, a);
}
