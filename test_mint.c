#include "mint.c"

int main(){
	m_uint a=M_UINT_INIT;
	unsigned int _a[2]={2062207102, 2984164923};
	re_set_muint(&a, 2, (unsigned int *)&_a);

	m_uint b=M_UINT_INIT;
	unsigned int _b[2]={2245483717, 653552764};
	re_set_muint(&b, 2, (unsigned int *)&_b);
	unsigned long long i=(unsigned long long)(_b[0])*((unsigned long long)(1)<<(unsigned long long)(sizeof(unsigned int)*8))+(unsigned long long)(_b[1]);
	
	unsigned long long n=(unsigned long long)(_a[0])*((unsigned long long)(1)<<(unsigned long long)(sizeof(unsigned int)*8))+(unsigned long long)(_a[1]);
	
	print(a);
	printf("\n");
	print(b);
	printf("\n\n");
	m_uint c=M_UINT_INIT;
	
	printf("xor\n");
	muint_xor(&c, a, b);
	print(c);
	printf("\n%llu\n\n", n^i);
	
	printf("and\n");
	muint_and(&c, a, b);
	print(c);
	printf("\n%llu\n\n", n&i);
	
	printf("or\n");
	muint_or(&c, a, b);
	print(c);
	printf("\n%llu\n\n", n|i);
	
	printf("rshift1\n");
	muint_rshift1(&c, c);
	print(c);
	printf("\n%llu\n\n", (n|i) << 1);
	
	printf("lshift1\n");
	muint_lshift1(&c, b);
	print(c);
	printf("\n%llu\n\n", i >> 1);
	
	printf("false\n");
	//printf("%llu\n", muint_false(d));
	printf("%llu\n", muint_false(a));
	//printf("%llu\n\n", muint_false(e));
	
	printf("add\n");
	muint_add(&c, a, b);
	print(c);
	printf("\n%llu\n\n", n+i);
	
	printf("sub\n");
	muint_sub(&c, b, a);
	print(c);
	printf("\n%llu\n\n", i-n);
	
	printf("mul\n");
	muint_mul(&c, a, b);
	print(c);
	printf("\n%llu\n\n", n*i);
	
	printf("div\n");
	muint_div(&c, b, a);
	print(c);
	printf("\n%llu\n\n", i/n);
	
	printf("mod\n");
	muint_mod(&c, b, a);
	print(c);
	printf("\n%llu\n", i%n);
}
