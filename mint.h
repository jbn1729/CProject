//definition
#define max(a, b) ((a)>(b)?(a):(b))
#define min(a, b) ((a)<(b)?(a):(b))
#define M_UINT_INIT {NULL,0};

//struct
typedef struct M_uint{
	unsigned int* blocks;
	int len;
}m_uint;
typedef struct M_int{
	int signe;	
	
}m_int;
//util
extern unsigned int ilen10(unsigned int n);

//redefinition
extern void re_set_muint(m_uint* rep, int len, unsigned int* list);
extern void muint_copy(m_uint* a, m_uint b);
extern struct M_uint set_uint_muint(unsigned int n);
extern void set_to_zero(m_uint* n);
extern void set_to_x(m_uint* n, unsigned int x);

//visuel
extern void print(struct M_uint n);

//opérateurs binaires
extern void muint_xor(struct M_uint* res_end, struct M_uint b, struct M_uint a);
extern void muint_and(struct M_uint* res_end, struct M_uint b, struct M_uint a);
extern void muint_or(struct M_uint* res_end, struct M_uint b, struct M_uint a);
extern void muint_rshift1(struct M_uint* res_end, struct M_uint a);
extern void muint_rshift(struct M_uint* res_end, struct M_uint b, unsigned int a);
extern void muint_lshift1(struct M_uint* res_end, struct M_uint a);
extern void muint_lshift(struct M_uint* res_end, struct M_uint b, unsigned int a);

//opérateurs sur le domaine dans {0, 1}
extern int muint_false(m_uint n);
extern int muint_lt(struct M_uint a, struct M_uint b);
extern int muint_le(struct M_uint a, struct M_uint b);
extern int muint_eq(struct M_uint a, struct M_uint b);

//opérateurs arithmétiques
extern void muint_sub(struct M_uint* res_end, struct M_uint x, struct M_uint y);
extern void muint_add(struct M_uint* res_end, struct M_uint x, struct M_uint y);
extern void muint_mul(struct M_uint* res_end, struct M_uint x, struct M_uint y);
extern void muint_div(struct M_uint* res_end, struct M_uint a, struct M_uint b);
extern void muint_mod(struct M_uint* res_end, struct M_uint a, struct M_uint b);

