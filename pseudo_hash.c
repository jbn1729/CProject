#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
#include "rand.c"
typedef unsigned __int128 uint128_t;
/*exemple de commande :
	./a.out 123 456 789
	donne le nombre à hasher : (123<<256)+(456<<128)+(789) -> 14242426976189892037099231156068612666107376873189737316192581653962201250136853
	le hash résultant est un int256
*/

unsigned int compress(uint128_t n){
	unsigned int res=0;
	const uint32_t m = (1ull<<16)-1;
	for(int i=0; i<8; i++){
		res ^= n&m;
		n >>= 16;
	}
	return res;
}

int main(int argc, char* argv[]){
	unsigned int n[argc];
	char* plus;
	for(int i=1; i<argc; i++){
		n[i-1] = strtoull(argv[i], &plus, 10);
	}
	n[argc-1] = 1;
	uint128_t res[2] = {0, 0};
	for(int i=0; i<argc; i++){
		//printf("%llu\n", n[i]);
		res[1] ^= n[i];
		unsigned int res_compress = compress(res[0])^compress(res[1]);
		struct rand rd=rand_seed(res_compress);
		uint128_t r0=0, r1=0;
		for(int j=0; j<128; j++){
			r0 = (r0<<1)^(rand_uint32(&rd)&1);
		}
		for(int j=0; j<128; j++){
			r1 = (r1<<1)^(rand_uint32(&rd)&1);
		}
		res[0] ^= r0;
		res[1] ^= r1;
	}
	char buffer[200];
	uint128_t mod=(1llu<<32)-1;
	sprintf(buffer, "python3 print_uint256.py %llu %llu %llu %llu", res[0]>> 32, res[0]&mod, res[1]>>32, res[1]&mod);
	system(buffer);
	//printf("%llu\n", res);
}
