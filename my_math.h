unsigned int mulmod_uint(unsigned int a, unsigned int b, unsigned int m);
unsigned int powmod_uint(unsigned int b, unsigned int e, unsigned int m);

unsigned long long mulmod_ull(unsigned long long a, unsigned long long b, unsigned long long m);
unsigned long long powmod_ull(unsigned long long b, unsigned long long e, unsigned long long m);
