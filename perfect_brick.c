#include <stdio.h>
#include "is_square.c"
#include "divisors.c"
#include "factorize_include.c" // positive_number = unsigned long long int

void yield_2d(positive_number* x, positive_number* y){
	if((*x) - (*y) > 4){
		*x = (*x)-2;
		*y = (*y)+2;
	}else{
		*x = (*x)+(*y);
		*y = 2;
	}
	//if(*x == *y)
	//	yield_2d(x, y);
}
/*int compareEntiers(const void* a, const void* b){
   return *((int*) a) - *((int*) b);
}*/
int main(){
	positive_number p=2, q=2;
	positive_number* factors = calloc(64, sizeof(positive_number));
	while(1){
		yield_2d(&p, &q);
		//printf("p=%llu q=%llu\n", p, q);
		positive_number pp=p*p, qq=q*q;
		positive_number a=pp-qq, b=p*q << 1;
		positive_number aa = a*a;
		positive_number aabb = aa+b*b;
		if(!(b&3)){
			positive_number* DIV;
			factor(b, factors);
			int len;
			//printf("%d %d\n", a, b);
			for(len=0; factors[len]; len++);
			qsort(factors, len, sizeof(positive_number), compareEntiers);
			int lendiv = divisor(&DIV, factors, len);
			for(int i=0; i<lendiv; i++){
				positive_number _d=DIV[i];
				positive_number _c=b/_d;
				if(_d > _c)break;
				else if(_d&1 || _c&1)continue;
				positive_number p2=_c+_d>>1, q2=_c-_d>>1;
				positive_number cs2 = p2*q2;
				if(!cs2)continue;
				positive_number cc=cs2*cs2 << 2;
				if(is_square(aa+cc)){
					if(is_square(aabb+cc)){
						printf("perfect : a=%llu b=%llu c=%llu p=%llu q=%llu p2=%llu q2=%llu\n", a, b, cs2 << 1, p, q, p2, q2);
						exit(0);
					}else{
						printf("almost : a=%llu b=%llu c=%llu p=%llu q=%llu p2=%llu q2=%llu\n", a, b, cs2<<1, p, q, p2, q2);
					}
				}
				
			}
			free(DIV);
		}
	}
}
