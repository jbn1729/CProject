//fichier pour tester

#include "newton_fractal.c"

void creal_poly(double* polyc, double complex* poly, unsigned int lpoly){
    for(int i=0; i<lpoly; i++){
        polyc[i] = creal(poly[i]);
    }
}

void printfpoly(double complex* poly, int lpoly){
    for(int i=0;i<lpoly;i++){
        printf("%Zg ", poly[i]);
    }
    printf("\n");
}

int main(){
    //initialisation
    double complex poly[3];
    poly[0] = CMPLX(5, 0);
    poly[1] = CMPLX(4, 0);
    poly[2] = CMPLX(3, 0);
    unsigned int lpoly = 3;
    double complex poly2[4];
    poly2[0] = CMPLX(4, 0);
    poly2[1] = CMPLX(3, 0);
    poly2[2] = CMPLX(2, 0);
    poly2[3] = CMPLX(1, 0);
    unsigned int lpoly2 = 4;
    printf("%Zg %Zg %Zg %Zg\n", poly[0], poly[1], poly[2], poly[3]);
    for(int i=0;i<4;i++){
        printf("%Zg ", poly2[i]);
    }
    printf("\n");
    double complex inter[4];
    double complex roots[3];
    roots[0] = CMPLX(7, 0);
    roots[1] = CMPLX(5, 0);
    roots[2] = CMPLX(3, 0);
    printf("%lf\n", cabs(CMPLX(3, 4)));
    unsigned int nbroots=3;
    //evalPC
    double complex res=evalPC(poly, CMPLX(3, 0), lpoly);
    printf("%Zg\n44\n\n", res);
    //addpoly
    addpoly(inter, poly, poly2, lpoly, lpoly2);
    printfpoly(inter, 4);
    printf("9 7 5 1\n\n");
    //mulpolyx
    mulpolyx(inter, poly, lpoly);
    printfpoly(inter, 4);
    printf("0 5 4 3\n\n");
    //oppoly
    oppoly(inter, poly2, lpoly2);
    printfpoly(inter, 4);
    printf("-4 -3 -2 -1\n\n");
    //copypoly
    copypoly(inter, poly2, lpoly2);
    printfpoly(inter, 4);
    printf("4 3 2 1\n\n");
    //mulpoly
    mulpoly(inter, poly, CMPLX(3, 0), lpoly);
    printfpoly(inter, 4);
    printf("-15 -7 -5 3\n\n");
    //create_poly
    create_poly(inter, roots, nbroots);
    printfpoly(inter, 4);
    printf("-105 71 -15 1\n\n");
    //derivate
    derivate(inter, poly2, lpoly2);
    printfpoly(inter, 3);
    printf("3 4 3\n\n");
}
