#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/sha.h>

void llu_to_char(unsigned long long n, unsigned char* R){
    for (int i = 0; i < 8; ++i)
        R[i] = (unsigned char)((n >> (56 - (i<<3))) & 0xFFu);
}
void sha256(unsigned long long n, unsigned char* hash){
	unsigned char data[8];
	llu_to_char(n, data);
	SHA256(data, strlen((char *)(&data)), hash);
}

int main(int argc, char* argv[]){
	char *eptr;
	unsigned long long key = strtoull(argv[2], &eptr, strlen(argv[2]));
	printf("%llu\n", key);
	char chiffre=1;
	if(argc > 4)
		chiffre=0;

	FILE *fp;
	long lSize;
	unsigned char *buffer;

	fp = fopen ( argv[1] , "rb" );
	if( !fp ) perror(argv[1]),exit(1);

	fseek( fp , 0L , SEEK_END);
	lSize = ftell( fp );
	rewind( fp );

	/* allocate memory for entire content */
	buffer = calloc( 1, lSize+1 );
	if( !buffer ) fclose(fp),fputs("memory alloc fails",stderr),exit(1);

	/* copy the file into the buffer */
	if( 1!=fread( buffer , lSize, 1 , fp) )
	  fclose(fp),free(buffer),fputs("entire read fails",stderr),exit(1);

	/* do your work here, buffer is a string contains the whole text */

	fclose(fp);
	printf("%d\n", lSize);
	#pragma omp parallel for
	for(int i=0; i<lSize; i++){
		unsigned char c = buffer[i];
		unsigned char hash[SHA256_DIGEST_LENGTH];
		sha256(key+i, hash);
		if(chiffre)
			c += hash[SHA256_DIGEST_LENGTH-1];
		else
			c -= hash[SHA256_DIGEST_LENGTH-1];
		buffer[i] = c;
	}
	// Char arrays are declared like so:

	// Open a file for writing. 
	// (This will replace any existing file. Use "w+" for appending)
	FILE *file = fopen(argv[3], "wb");

	size_t r1 = fwrite(buffer, sizeof(buffer[0]), lSize, file);
	printf("%d\n", r1);
	fclose(file);
	free(buffer);
}
