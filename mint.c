#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mint.h"

/*typedef struct M_uint{
	unsigned int* blocks;
	int len;
}m_uint;*/

unsigned int ilen10(unsigned int n){
	return (unsigned int)(log(2)*n/log(10));
}
void re_set_muint(m_uint* rep, int len, unsigned int* list){
	int i=0;
	while(i<(len-1) && list[i] == 0){
		i++;
	}
	rep->len = len-i;
	rep->blocks=(unsigned int*)realloc((void*)rep->blocks, sizeof(unsigned int)*(len-i));
	for(int j=i; j<len; j++){
		rep->blocks[j] = list[j];
	}
}
void set_to_zero(m_uint* n){
	unsigned int zero[1]={0};
	re_set_muint(n, 1, (unsigned int*)&zero);
}
void set_to_x(m_uint* n, unsigned int x){
	unsigned int _x[1]={x};
	re_set_muint(n, 1, (unsigned int*)&_x);
}
void muint_copy(m_uint* a, m_uint b){
	re_set_muint(a, b.len, b.blocks);
}


struct M_uint set_uint_muint(unsigned int n){
	m_uint base=M_UINT_INIT;
	base.len=1;
	base.blocks=realloc(NULL, sizeof(unsigned int));
	base.blocks[0] = n;
	return base;
}

void print(struct M_uint n){
	m_uint n_copy=M_UINT_INIT;
	m_uint c=M_UINT_INIT;
	muint_copy(&n_copy, n);
	m_uint dix=M_UINT_INIT;
	unsigned int dix_[1]={10};
	re_set_muint(&dix, 1, (unsigned int*)&dix_);
	unsigned int len=ilen10(n.len*sizeof(unsigned int)*8)+1;
	char form[len];
	if(n.len==1)printf("%u\n", n.blocks[0]);
	if(n.len==2)printf("%llu\n", ((unsigned long long)n.blocks[0]<<((unsigned long long)(sizeof(unsigned int)*8)))+(unsigned long long)n.blocks[1]);
	else printf("%u %u %u\n", n.blocks[0], n.blocks[1], n.blocks[2]);
	printf("%d %d\n", len, n.len);
	int i;
	for(i=len-1; i>=0 && muint_false(n_copy); i--){
		muint_mod(&c, n_copy, dix);
		printf("%u %d\n", c.blocks[0], c.len);
		form[i]=(char)(c.blocks[0]);
		muint_div(&n_copy, n_copy, dix);
	}
	for(int j=i; j<len; j++){
		printf("%d ", form[j]);
	}
}
	

void muint_xor(struct M_uint* res_end, struct M_uint b, struct M_uint a){
	unsigned int len=max(b.len, a.len);

	unsigned int res[len];
	for(int i=len-min(b.len, a.len); i<len; i++){
		res[i]=b.blocks[i]^a.blocks[i];
	}
	unsigned int* blocks = a.len>b.len?a.blocks:b.blocks;
	for(int i=0; i<len-min(b.len, a.len); i++)
		res[i] = blocks[i];
	re_set_muint(res_end, len, (unsigned int *)&res);
}
void muint_and(struct M_uint* res_end, struct M_uint b, struct M_uint a){
	unsigned int len=min(b.len, a.len);
	unsigned int res[len];
	for(int i=0; i<min(b.len, a.len); i++){
		res[i]=b.blocks[i]&a.blocks[i];
	}
	re_set_muint(res_end, len, (unsigned int *)&res);
}
void muint_or(struct M_uint* res_end, struct M_uint b, struct M_uint a){
	unsigned int len=max(b.len, a.len);
	unsigned int res[len];
	for(int i=len-min(b.len, a.len); i<len; i++){
		res[i]=b.blocks[i]|a.blocks[i];
	}
	unsigned int* blocks = a.len>b.len?a.blocks:b.blocks;
	for(int i=0; i<len-min(b.len, a.len); i++)
		res[i] = blocks[i];
	re_set_muint(res_end, len, (unsigned int *)&res);
}
void muint_rshift1(struct M_uint* res_end, struct M_uint a){
	unsigned int len=a.len;
	unsigned int len_end = len;
	unsigned char dec=0;
	unsigned int len_uint=sizeof(unsigned int)*8-1;
	if((a.blocks[0]>>len_uint) > 0){
		len_end++;
		dec=1;
	}
	unsigned int res[len_end];
	unsigned int mem=0;
	for(int i=len-1; i>=0; i--){
		unsigned int current=a.blocks[i];
		res[i+dec] = (current<<1)|mem;
		mem=current>>len_uint;
	}
	if(dec)res[0]=1;
	re_set_muint(res_end, len_end, (unsigned int *)&res);
}
void muint_rshift(struct M_uint* res_end, struct M_uint b, unsigned int a){
	muint_copy(res_end, b);
	for(int i=0; i<a; i++){
		muint_rshift1(res_end, *res_end);
	}
}
void muint_lshift1(struct M_uint* res_end, struct M_uint a){
	unsigned int len_end = a.len;
	unsigned int len_uint=sizeof(unsigned int)*8-1;
	unsigned int res[len_end];
	unsigned int mem=0;
	for(int i=0; i<a.len; i++){
		unsigned int current=a.blocks[i];
		res[i] = (current>>1)|(mem<<len_uint);
		mem=current&1;
	}
	re_set_muint(res_end, len_end, (unsigned int *)&res);
}
void muint_lshift(struct M_uint* res_end, struct M_uint b, unsigned int a){
	muint_copy(res_end, b);
	for(int i=0; i<a; i++){
		muint_lshift1(res_end, *res_end);
	}
}
int muint_false(m_uint n){
	for(int i=0;i<n.len; i++){
		if(n.blocks[i] != 0)return 1;
	}
	return 0;
}
int muint_lt(struct M_uint a, struct M_uint b){
	if(a.len<b.len)return 0;
	else if(a.len>b.len)return 1;
	for(int i=0; i<a.len; i++){
		if(a.blocks[i]<b.blocks[i])return 0;
		else if(a.blocks[i]>b.blocks[i])return 1;
	}
	return 0;
}
int muint_le(struct M_uint a, struct M_uint b){
	if(a.len<b.len)return 0;
	else if(a.len>b.len)return 1;
	for(int i=0; i<a.len; i++){
		if(a.blocks[i]<b.blocks[i])return 0;
		else if(a.blocks[i]>b.blocks[i])return 1;
	}
	return 1;
}
int muint_eq(struct M_uint a, struct M_uint b){
	if(a.len!=b.len)return 0;
	for(int i=0;i<a.len; i++){
		if(a.blocks[i] != b.blocks[i])return 0;
	}
	return 1;
}


void muint_sub(struct M_uint* res_end, struct M_uint x, struct M_uint y){
	if(muint_lt(y, x)){
		printf("obligation:x>y\n");
	}
	long long mem=0;
	long long size=(long long)1<<(long long)(sizeof(unsigned int)<<3);
	unsigned int res[x.len];
	int i;
	for(i=0; i<y.len; i++){
		long long res_i=(long long)x.blocks[x.len-i-1]-(long long)y.blocks[y.len-i-1]-mem;
		if(res_i<0){
			mem=1;
			res_i += size;
		}
		res[x.len-i-1]=(unsigned int)res_i;
	}
	for(i=y.len; i<x.len; i++){
		long long res_i=(long long)x.blocks[x.len-i]-mem;
		if(res_i<0){
			mem=1;
			res_i += size;
		}
		res[x.len-i]=(unsigned int)res_i;
		//printf("%u\n", res[x.len-i]);
	}	
	re_set_muint(res_end, x.len, (unsigned int *)&res);
}

void muint_add(struct M_uint* res_end, struct M_uint x, struct M_uint y){
	m_uint a=M_UINT_INIT;
	m_uint b=M_UINT_INIT;
	m_uint x_copy=M_UINT_INIT;
	m_uint y_copy=M_UINT_INIT;
	muint_copy(&x_copy, x);
	muint_copy(&y_copy, y);
	do{
		muint_and(&a, x_copy, y_copy);
		muint_xor(&b, x_copy, y_copy);
		muint_rshift1(&x_copy, a);
		muint_copy(&y_copy, b);
	}while(muint_false(a));
	muint_copy(res_end, b);
}

void muint_mul(struct M_uint* res_end, struct M_uint x, struct M_uint y){
	m_uint x_copy=M_UINT_INIT;
	m_uint un=M_UINT_INIT;
	m_uint y_copy=M_UINT_INIT;
	m_uint rep=M_UINT_INIT;
	muint_copy(&x_copy, x);
	muint_copy(&y_copy, y);
	unsigned int zero[1]={0};
	unsigned int un_[1]={1};
	re_set_muint(res_end, 1, (unsigned int *)&zero);
	re_set_muint(&un    , 1, (unsigned int *)&un_);
	do{
		muint_and(&rep, x_copy, un);
		if(muint_false(rep))
			muint_add(res_end, y_copy, *res_end);
		muint_rshift1(&y_copy, y_copy);
		muint_lshift1(&x_copy, x_copy);
	}while(muint_false(x_copy));
}
void muint_div(struct M_uint* res_end, struct M_uint a, struct M_uint b){
	m_uint q = M_UINT_INIT;
	unsigned int zero[1]={0};
	re_set_muint(&q, 1, (unsigned int *)&zero);
	unsigned int un_[1]={1};
	m_uint un = M_UINT_INIT;
	re_set_muint(&un, 1, (unsigned int *)&un_);
	m_uint a_copy=M_UINT_INIT;
	m_uint b_copy=M_UINT_INIT;
	muint_copy(&a_copy, a);
	muint_copy(&b_copy, b);
	if(!muint_false(b_copy)){
		printf("div by zero\n");
		return ;
	}
	int dec=0;
	while(muint_lt(a_copy, b_copy)){
		dec++;
		muint_rshift1(&b_copy, b_copy);
	}
	muint_lshift1(&b_copy, b_copy);
	for(int j=0; j<dec; j++){
		muint_rshift1(&q, q);
		if(muint_lt(a_copy, b_copy)){
			muint_sub(&a_copy, a_copy, b_copy);
			muint_or(&q, q, un);
		}
		muint_lshift1(&b_copy, b_copy);
	}
	muint_copy(res_end, q);
}
void muint_mod(struct M_uint* res_end, struct M_uint a, struct M_uint b){
	m_uint a_copy=M_UINT_INIT;
	m_uint b_copy=M_UINT_INIT;
	muint_copy(&a_copy, a);
	muint_copy(&b_copy, b);
	if(!muint_false(b_copy)){
		printf("mod by zero\n");
		return ;
	}
	int dec=0;
	while(muint_le(a_copy, b_copy)){
		dec++;
		muint_rshift1(&b_copy, b_copy);
	}
	if(!dec){
		set_to_zero(res_end);
		return;
	}
	muint_lshift1(&b_copy, b_copy);
	for(int j=0; j<dec; j++){
		if(muint_le(a_copy, b_copy)){
			muint_sub(&a_copy, a_copy, b_copy);
		}
		muint_lshift1(&b_copy, b_copy);
	}
	muint_copy(res_end, a_copy);
}
/*typedef struct M_int{
	int signe;	
	
}m_int;*/
