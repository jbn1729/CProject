/* If x is square, return 1.
 * Otherwise, return 0.
 */
 //https://math.stackexchange.com/questions/41337/efficient-way-to-determine-if-a-number-is-perfect-square
char is_square(unsigned long long x) {
    /* Precondition: make x odd if possible. */
    int sh = __builtin_ctzll(x);
    x >>= (sh&~1);

    /* Early escape. */
    if (x&6) return 0;

    /* 2-adic Newton */
    int i;
    const int ITERATIONS = 5; // log log x - 1
    unsigned long long z = (3-x)>>1, y=x*z;
    for (i=0; i<ITERATIONS; i++) {
        unsigned long long w = (3 - z*y) >> 1;
        y *= w;
        z *= w;
    }
    //assert(x==0 || (y*z == 1 && x*z == y));

    /* Get the positive square root.  Also the top bit
     * might be wrong. */
    if (y & (1ull<<62)) y = -y;
    y &= ~(1ull<<63);

    /* Is it the square root in Z? */
    if (y >= 1ull<<32) return 0;

    /* Yup. */
    return 1;
}
