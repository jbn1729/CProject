# language C

programme à compiler | commande | utilisation | librairies utilisées
--- | --- | --- | ---
saving image.c | gcc saving_image.c -I/usr/include/libpng16 -lpng16 -O3 -lm -ggdb -fopenmp | ./a.out largeur hauteur nombre_de_racines | libpng, math, openmp
mon_gtk.c | gcc -o mon_gtk.out mon_gtk.c $( pkg-config --libs gtk4 ) $( pkg-config --cflags gtk4 ) | non utilisable | gtk4
neural_network.c | gcc neural_network.c -lm | non utilisable | math
factorize.c | gcc factorize.c $( pkg-config --libs openssl ) | ./a.out | openssl
chiffre_fichier.c | gcc chiffre_fichier.c $( pkg-config --libs openssl ) | ./a.out nom_du_fichier clé nom_du_fichier_chiffré argument_en_plus_si_déchiffrement | openssl
chiffre_fichier_parallel.c | gcc chiffre_fichier_parallel.c $( pkg-config --libs openssl ) -fopenmp | ./a.out nom_du_fichier clé nom_du_fichier_chiffré argument_en_plus_si_déchiffrement | openssl omp
syracuse_fast.c | gcc syracuse_fast.c -fopenmp -O3 $(pkg-config --libs openssl gmp) | ./a.out | gmp omp
pseudo_hash.c | gcc pseudo_hash.c -O3 | ./a.out 123 456 789 (example) | aucune
