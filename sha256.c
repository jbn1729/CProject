#include <openssl/sha.h>
#include <stdio.h>
#include <string.h>

int main() {

	unsigned char data[] = "some text";
	unsigned char hash[SHA256_DIGEST_LENGTH];
	SHA256(data, strlen((char *)data), hash);

	for (int i = 0; i < SHA256_DIGEST_LENGTH; i++)
		printf("%d\n", hash[i]);
}
