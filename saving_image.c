#include "newton_fractal.c"
#include "create_png.c"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
//#include <time.h>
/*img = [[[0, 0, 0], [0, 0, 0], [0, 0, 0]],
         [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
         [[0, 0, 0], [0, 0, 0], [0, 0, 0]]]
inputs img = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
*/
void saving_img(Liste1D* img, const char* file, unsigned int width, unsigned int height){
    liste1D_to_img(img->list, file, width, height);
    /*FILE* f = fopen(file, "wb");
    fprintf(f, "P6\n%i %i 255\n", width, height);
    unsigned ijk = 0;
    for (int y=0; y<height; y++) {
        for (int x=0; x<width; x++) {
            fputc(img->list[ijk  ], f);  // 0 .. 255
            fputc(img->list[ijk+1], f);  // 0 .. 255
            fputc(img->list[ijk+2], f);  // 0 .. 255
            ijk += 3;
        }
    }
    fclose(f);*/
}

void printf_l(char* argv, int argc){
    for(int i=0; i<argc; i++){
        printf("%s\n", argv[i]);
    }
}
void printf_time(double sec){
    if(sec > 60){
        int min = (int)sec/60;
        sec -= (double)(min*60);
        if(min > 60){
            int h = min/60;
            min -= h*60;
            if(h > 24){
                int j = min/24;
                h -= j*24;
                printf("%dj %dh %d min %lf sec", j, h, min, sec);
            }else{
                printf("%dh %d min %lf sec", h, min, sec);
            }
        }else{
            printf("%d min %lf sec", min, sec);
        }
    }else{
        printf("%lf sec", sec);
    }
}
int main(int argc, char** argv){
    char **ptr;
    unsigned int nbroots=8;
    unsigned int width=1000, height=1000;
    double complex a=CMPLX(1, 0);
    int i=0;
    for (ptr = argv; *ptr != NULL; ptr++){
        if(i==1)width = atoi(*ptr);
        else if(i==2)height = atoi(*ptr);
        else if(i==3)nbroots = atoi(*ptr);
        else if(i==4)a = CMPLX(atoi(*ptr), atoi(*(ptr+1)));
        i++;
    }
    printf("width = %d\nheight = %d\nnbroots = %d\n", width, height, nbroots);
    double dec_x = (double)width /2.0,
           dec_y = (double)height/2.0,
           scale = sqrt(width*height)/5;
    Liste1D* F;
    F = (Liste1D*)malloc(sizeof(Liste1D));
    F->length = width*height*3;
    printf("début de la génération d'image\n");
    clock_t t1, t2;
    long clk_tck = CLOCKS_PER_SEC;
    t1 = clock();
    fractal_roots(F, nbroots, width, height, scale, dec_x, dec_y, a);
    t2 = clock();
    printf("Temps de la génération de l'image : ");
    printf_time((double)(t2-t1)/(double)clk_tck);
    printf("\nenrgistrement de l'image %lf %lf %lf\n", t2-t1, clk_tck, (double)(t2-t1)/(double)clk_tck);
    saving_img(F, "out.png", width, height);
    
}
