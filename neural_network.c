/*sources : https://www.delftstack.com/fr/howto/c/read-file-c/

*/

#include <stdio.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#define MAX_NEURONES 100000
#define MAX_LAYERS 100
typedef struct{
    unsigned int x, y;
    double mat[MAX_NEURONES][MAX_NEURONES];
}mat;

typedef struct{
    unsigned int size[MAX_NEURONES];
    mat weight[MAX_LAYERS-1];
    double biases[MAX_LAYERS-1][MAX_NEURONES];
    unsigned int n_layers;
}network;

void init(network* neural, unsigned int* size, unsigned int n_layers){
    neural->n_layers = n_layers;
    for(int i=0;  i<n_layers; i++){
        neural->size[i] = size[i];
    }
}

void dot(double* res, double* mat1, mat* mat2){
     unsigned int j,k;
     for (j=0;j<=mat2->y;j++){
         res[j]=0;
         for (k=0;k<mat2->x;k++)
               res[j]+=mat1[k]*mat2->mat[k][j];
    }
}

void sigmo(double* res, double* inputs, unsigned int linputs){
    for(int i=0; i<linputs; i++)
        res[i] = 1/(exp(inputs[i])+1);
}

void add(double* res, double* plu, unsigned int lplu){
    for(int i=0; i<lplu; i++)
        res[i] = plu[i];
}

void copy(double* res, double* c, unsigned int lc){
    for(int i=0; i<lc; i++){
        res[i] = c[i];
    }
}

void eval_network(double* res, double* x, network* neural){
    copy(res, x, neural->weight->x);
    for(int i=0; i<neural->n_layers; i++){
        dot(res, res, &neural->weight[i]);
        add(res, neural->biases[i], neural->weight->y);
        sigmo(res, res, neural->weight->y);
    }
}

void GPT(double* res, double* inputs, unsigned int n_tokens){
    unsigned char equal[n_tokens];
    //...
}

int main(int argc, char** argv){
    //prendre les paramètres : nombre de couches, nombre de neurones à haque fois
    //nombre de réseau pour l'algo génétique
    unsigned int n_layer;
    printf("nombre dee couches : ");
    scanf("%d", &n_layer);
    unsigned int size[n_layer];
    int i;
    for(i=0; i<n_layer; i++)
        printf("nombre de neurones dans la couche n°%d : ", i+1);
        scanf("%d", &size[i]);
    unsigned int n_neural;
    printf("nombre de réseaux de neurones : ");
    scanf("%d", &n_neural);
    //lire un fichier
    const char* filename = "paragraphes.txt";

    FILE* input_file = fopen(filename, "r");
    if (!input_file)
        exit(EXIT_FAILURE);

    struct stat sb;
    if (stat(filename, &sb) == -1) {
        perror("stat");
        exit(EXIT_FAILURE);
    }

    char* file_contents = malloc(sb.st_size);
    fread(file_contents, sb.st_size, 1, input_file);

    //printf("%s\n", file_contents);

    fclose(input_file);
    free(file_contents);

    exit(EXIT_SUCCESS);
}
