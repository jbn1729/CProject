#include <math.h>
#include <complex.h>
#include <stdlib.h>
#include <stdio.h>
#define MAXI(a, b) a<b?b:a
#define MINI(a, b) a<b?a:b
#define IMAX(a, b) a<b?1:0
#define MAXW 16384
#define MAXH 16384
#define DIST_MAX 65536
#define PI 3.14159265358979323846264338327950
//liste 1D et liste 3D

typedef struct{
    unsigned char list[MAXW*MAXH*3];
    unsigned int length;
}Liste1D;
typedef struct{
    unsigned char list[MAXW][MAXH][3];
    unsigned int maxw;
    unsigned int maxh;
}Liste3D;
//nombres complexes
#define UN CMPLX(1, 0)
#define ZE CMPLX(0, 0)
//general
double complex general(double complex c){
    return ccosh(z)-1
}
double complex derivate_general(double complex c){
    return sinh(c)
}
double complex stape(double complex c, double complex a){
    return c-a*general(c)/derivate_general(c)
}
//newton
double complex newton(double complex c, unsigned int maxit, double complex a){
    double complex cav;
    unsigned int i=0;
    while(1){
        cav = c,
        //c-= newton_fonc(c, roots, lroots);
        c = stape(c, a);
        if (cabs(c-cav)<1e-11) return i;
        i++;
    }
}

double dist(double complex c1, double complex c2){
    return cabs(c1-c2);
}
unsigned int iclose(double complex cp, double complex* cs, unsigned int lcs){
    double distmin = DIST_MAX, distpoint;
    int imin=0;
    for(int i=0; i<lcs; i++){
        distpoint = dist(cp, cs[i]);
        if(distpoint < distmin){
            imin = i;
            distmin = distpoint;
        }
    }
    return imin;
}
unsigned int color_point(double x, double y, unsigned int lpoly, double complex* roots, unsigned int lroots, double complex* H, double complex* B){
    return iclose(newton(CMPLX(x, y), 100, lpoly, H, B, roots, lroots), roots, lroots);
}
//fractal
void copy_liste1D(Liste1D* F, Liste1D* Fc){
    F->length = Fc->length;
    for(int i=0; i<(F->length); i++){
        F->list[i] = Fc->list[i];
    }
}

void remplis(Liste1D* F, unsigned char r, unsigned int imin){
    for(unsigned int i=imin; i<F->length; i++){
        F->list[i] = r;
    }
}

void fractal(  Liste1D* F, unsigned int width, unsigned int height, 
                        double scale, double dec_x, double dec_y,
                        double complex* poly,  unsigned int lpoly,
                        double complex* roots, unsigned int lroots,
                        double complex a){
    double x, y;
                                                    
    unsigned char COLOR[8][3] = {   {255, 0  , 0  },   //#FF0000
                                    {0  , 255, 0  },   //#00FF00
                                    {0  , 0  , 255},   //#0000FF
                                    {255, 255, 0  },   //#FFFF00
                                    {0  , 255, 255},   //#00FFFF
                                    {255, 0  , 255},   //#FF00FF
                                    {255, 255, 255},   //#FFFFFF
                                    {0  , 0  , 0  }};  //#000000
    unsigned int rvbi;
    double complex H[lpoly],
                   B[lpoly-1];
    create_simplPoD(H, B, poly, lpoly);
    #pragma omp parallel for
    for(int i=0; i<width; i++){
        unsigned int ijk=i*height*3;
        x = (i-dec_x)/scale;
        for(int j=0; j<height; j++){
            y = (j-dec_y)/scale;
            rvbi = color_point(x, y, lpoly, roots, lroots, H, B);
            F->list[ijk] = COLOR[rvbi][0];
            F->list[ijk+1] = COLOR[rvbi][1];
            F->list[ijk+2] = COLOR[rvbi][2];
            ijk += 3;
        }
    }
}
void fractal_roots(Liste1D*F, unsigned int nbroots, unsigned int width, unsigned int height, double scale, double dec_x, double dec_y, double complex a){
    double complex pi2i = CMPLX(0, PI*2);//pi*2i
    double complex roots[nbroots];
    F->length = width*height*3;
    for(unsigned int i=0; i < nbroots; i++){
        roots[i] = cexp(pi2i*i/nbroots);
    }
    double complex poly[nbroots+1];
    create_poly(poly, roots, nbroots);
    unsigned int lpoly=nbroots+1;
    fractal(F, width, height, scale, dec_x, dec_y, poly, lpoly, roots, nbroots, a);
}
