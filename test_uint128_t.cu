#include "uint128_or_uint256.cu"
#include <stdio.h>
#include <time.h>
#include<unistd.h>
static void HandleError( cudaError_t err,
						 const char *file,
						 int line ) {
	if (err != cudaSuccess) {
		printf( "%s in %s at line %d\n", cudaGetErrorString( err ),
				file, line );
		exit( EXIT_FAILURE );
	}
}
//https://dev.library.kiwix.org/content/stackoverflow_en_nopic_2021-08/questions/11656241/how-to-print-uint128-t-number-using-gcc
#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))
__global__ void test(uint128_t n, uint128_t* maxi){
	uint128_t res(0, 0), i2(0, 0);
	res.copy(n);
	printf("0 %llu %llu\n", n.hi, n.lo);
	printf("1 %llu %llu\n", res.hi, res.lo);
	while(res.cmp(n) < 1){
		n.odd_apply();
		//n.lshift1();
		printf("2 %llu %llu\n", n.hi, n.lo);
		if(n.is_even()){
			n.lshift1_stock(&i2);
			i2.setmaxin(maxi);
			while(n.is_even())n.rshift1();
		}
		//printf("2 %llu %llu\n", n.hi, n.lo);
	}
	printf("4 %llu %llu\n", n.hi, n.lo);
	printf("5 %llu %llu\n", res.hi, res.lo);
	//printf("6 %d\n", res.cmp(n));
}

int main(){
	uint128_t n(0, 27), maxi(0, 0);
	uint128_t* dev_maxi;
	HANDLE_ERROR(cudaMalloc((void**)&dev_maxi, sizeof(uint128_t)));
	cudaMemcpy(&dev_maxi, &maxi, sizeof(uint128_t), cudaMemcpyHostToDevice);
	printf("top\n");
	test<<<1, 1>>>(n, dev_maxi);
	cudaDeviceSynchronize();
	HANDLE_ERROR(cudaMemcpy(&maxi, dev_maxi, sizeof(uint128_t), cudaMemcpyDeviceToHost));
	printf("%s\n", cudaGetErrorString( cudaGetLastError()));
	printf("%llu %llu\n", maxi.hi, maxi.lo);
	/*long c = time(NULL);
	sleep(1);
	printf("%lu\n", time(NULL)-c);*/
}
