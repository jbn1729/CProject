#include <stdio.h>


#define MAX_X 14
#define MAX_Y 14


struct Image{
    int x, y;
    unsigned char img[MAX_X][MAX_Y][3];
};

void create_img(struct Image *img, int x, int y){
    img->x = x;
    img->y = y;
    for(int i=0; i < x;i++){
        for(int j=0; j < y; j++){
            printf("%d %d\n", i, j);
            img->img[i][j][0] = 0;
            img->img[i][j][1] = 0;
            img->img[i][j][2] = 0;
        }
    }
}

void print_img(struct Image *img){
    unsigned char r, v, b;
    for(int x=0; x<img->x; x++){
        for(int y=0; y<img->y; y++){
            r = img->img[x][y][0];
            v = img->img[x][y][1];
            b = img->img[x][y][2];
            printf("%d %d %d", r, v, b);
        }
    }
}

struct Image copy(struct Image cimg){
    struct Image new_img = {cimg.x, cimg.y};
    struct Image *new_pimg = &new_img;
    struct Image *pcimg = &cimg;
    for(int i=0; i<cimg.x; i++){
        for(int j=0; j<cimg.y; j++){
            new_pimg->img[i][j][0] = pcimg->img[i][j][0];
            new_pimg->img[i][j][1] = pcimg->img[i][j][1];
            new_pimg->img[i][j][2] = pcimg->img[i][j][2];
        }
    }
    return new_img;
}

int main(int argc, char *argv[]){
    printf("créé l'image\n");
    struct Image img;
    printf("remplis de 0\n");
    create_img(&img, 10, 10);
    print_img(&img);
    return 0;
}
