#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int compareEntiers(const void* a, const void* b){
   return *((int*) a) - *((int*) b);
}

unsigned long long add(unsigned long long** R, unsigned long long el, unsigned long long len){
	if(len){
		unsigned long long* Ri = calloc(len, sizeof(unsigned long long));
		for(int i=0; i<len; i++)Ri[i] = (*R)[i];
		(*R) = reallocarray((void*)(*R), (size_t)(len+1), sizeof(unsigned long long));
		if(*R == NULL){
			printf("error : %llu %llu\n", el, len);
			exit(1);
		}
		//printf("%llu %llu\n", el, len+1);
		for(int i=0; i<len; i++)(*R)[i] = Ri[i];
		(*R)[len] = el;
		free(Ri);
		return len+1;
	}else{
		if(((*R)=reallocarray(NULL, 1, sizeof(unsigned long long))) == NULL){
			printf("error : %llu %llu\n", el, len);
			exit(1);
		}
		*R[0] = el;
		return 1;
	}
}

int mul(unsigned long long* factors, unsigned long long** R, unsigned long long muld, int len, int lenR){
	if(!len)return lenR;
	unsigned long long fac = factors[0];
	unsigned long long all=muld*fac;
	int j;
	for(j=1; (j<len && factors[j]==fac); j++);
	lenR = mul(factors+j, R, muld, len-j, lenR);
	for(int i=0; i<j; i++){
		lenR = add(R, all, lenR);
		lenR = mul(factors+j, R, all, len-j, lenR);
		all *= fac;
	}
	return lenR;
}
int divisor(unsigned long long** R, unsigned long long* factors, int len){
	add(R, 1, 0);
	if(len==1){
		//printf("%llu\n", factors[0]);
		add(R, factors[0], 1);
		return 2;
	}
	int lend = mul(factors, R, 1, len, 1);
	qsort(*R, lend, sizeof(unsigned long long), compareEntiers);
	return lend;
}
