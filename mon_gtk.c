//https://www.developpez.net/forums/d1203975/c-cpp/c/code-couleur-c/#post6594329
#include <gtk/gtk.h>

void activate (GtkApplication* app, gpointer user_data){
    GtkWidget *window, *image;

    window = gtk_application_window_new (app);
    gtk_window_set_title (GTK_WINDOW (window), "Fractal");
    gtk_window_set_default_size (GTK_WINDOW (window), 500, 500);
    image = gtk_image_new_from_file ("out.png");
    gtk_window_set_child(window, image);
    gtk_widget_set_visible(window, (gboolean)TRUE);
    gtk_widget_set_visible(image, (gboolean)TRUE);
    printf("%d\n", gtk_widget_is_visible(image));
}

int main (int argc, char **argv){
    GtkApplication *app;
    int status;

    app = gtk_application_new ("org.gtk.example", G_APPLICATION_DEFAULT_FLAGS);
    g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
    status = g_application_run (G_APPLICATION (app), argc, argv);
    g_object_unref (app);

    return status;
}

