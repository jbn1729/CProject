#include <inttypes.h>
#define T63 ((uint64_t)1)<<63
struct uint128_t{
	uint64_t lo;
	uint64_t hi;
	__device__ __host__ uint128_t(uint64_t a, uint64_t b): hi(a), lo(b) {}
	__device__ void copy(uint128_t res){
		lo = res.lo;
		hi = res.hi;
	}
	__device__ void lshift1(void){
		hi <<= 1;
		if(lo>>63){
			hi ^= 1;
		}
		lo <<= 1;
	}
	__device__ void rshift1(void){
		//uint64_t T63 = ;
		lo >>= 1;
		if(hi&1){
			lo ^= T63;
		}
		hi >>= 1;
	}
	__device__ void add(uint128_t b){
		lo += b.lo;
		hi += b.hi + (lo < b.lo);
	}
	__device__ void add1(void){
		lo ++;
		if(!lo)hi++;
	}
	__device__ void odd_apply(void){
		uint128_t st(0, 0);
		st.lo = lo;
		st.hi = hi;
		rshift1();
		add(st);
		add1();
	}
	__device__ char is_even(void){
		return (lo&1) ^ 1;
	}
	__device__ char cmp(uint128_t b){
		if(hi > b.hi)return 1;
		else if(hi < b.hi)return -1;
		else{
			if(lo > b.lo)return 1;
			else if(lo < b.lo)return -1;	
			else return 0;
		}
	}
	__device__ void lshift1_stock(uint128_t* res){
		res->hi = hi<<1;
		if(lo>>63){
			res->hi ^= 1;
		}
		res->lo = lo << 1;
	}
	__device__ void setmaxin(uint128_t* res){
		if(hi > (*res).hi)
			(*res).hi = hi, (*res).lo=lo;
		else if(hi == (*res).hi && lo > (*res).lo)
			(*res).lo = lo;
	}
};
